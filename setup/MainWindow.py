# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtCore import *
#from PyQt5.QtCore import QTimer, QTime
import time
import os
import threading

from text_to_speech import Text_to_speech
from notify_window import Ui_MainWindow1


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1024, 600)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.centralwidget.setStyleSheet("border-color: rgb(255, 255, 255);")
        self.centralwidget.setStyleSheet("background-color: #8fd9b4")
        

        # button createfile
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(700, 100, 120, 50))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.setStyleSheet("background-color: #f0824b")
        self.pushButton.clicked.connect(self.createFile)

        # button listen
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(700, 200, 120, 50))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.setStyleSheet("background-color: #0d75f7")
        self.pushButton_2.clicked.connect(self.listenFile)

        # nhap noi dung
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(50, 100, 600, 250))
        self.textEdit.setObjectName("textEdit")
        self.textEdit.setStyleSheet("background-color: #ffffff")
        font = QtGui.QFont()
        font.setPointSize(22)
        self.textEdit.setFont(font)

        # nhap duong dan luu file
        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(130, 480, 550, 50))
        self.textEdit_2.setObjectName("textEdit_2")
        self.textEdit_2.setStyleSheet("background-color: #ffffff")
        font = QtGui.QFont()
        font.setPointSize(22)
        self.textEdit_2.setFont(font)

        # button save file
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(700, 300, 120, 50))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.setStyleSheet("background-color: #f1f184")
        self.pushButton_3.clicked.connect(self.saveFile)



        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(680, 480, 120, 50))
        self.pushButton_4.setAutoFillBackground(False)
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.setStyleSheet("background-color: #3be36a")
        self.pushButton_4.clicked.connect(self.getUrl)

        self.label_1 = QtWidgets.QLabel(self.centralwidget)
        self.label_1.setGeometry(QtCore.QRect(180, 50, 350, 50))
        font = QtGui.QFont()
        font.setPointSize(22)
        self.label_1.setFont(font)
        self.label_1.setObjectName("label_1")
        self.label_1.setStyleSheet("background-color: #f4fc59")
        self.label_1.setAlignment(QtCore.Qt.AlignCenter)

        self.textEdit_3 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_3.setGeometry(QtCore.QRect(130, 420, 550, 50))
        self.textEdit_3.setObjectName("textEdit_3")
        self.textEdit_3.setStyleSheet("background-color: #ffffff")
        font = QtGui.QFont()
        font.setPointSize(22)
        self.textEdit_3.setFont(font)


        self.textEdit_4 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_4.setGeometry(QtCore.QRect(20, 420, 100, 50))
        self.textEdit_4.setObjectName("textEdit_4")
        self.textEdit_4.setStyleSheet("background-color: #f1f184")

        font = QtGui.QFont()
        font.setPointSize(22)
        self.textEdit_4.setFont(font)

        self.textEdit_5 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_5.setGeometry(QtCore.QRect(20, 480, 100, 50))
        self.textEdit_5.setObjectName("textEdit_5")
        self.textEdit_5.setStyleSheet("background-color: #f5c958")
        font = QtGui.QFont()
        font.setPointSize(22)
        self.textEdit_5.setFont(font)

        #chon giong 
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(860, 60, 131, 31))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setStyleSheet("background-color: #e32061")
       
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(845, 100, 175, 31))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.comboBox.setFont(font)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.setStyleSheet("background-color: #ffffff")
        self.comboBox.addItem("")
        self.comboBox.addItem("")

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1024, 25))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        #khoi tao bien
        self.TTS_gui = Text_to_speech()
        self.voice = ''
        self.voice_new = ''
        self.url = ''
        self.url_new = ''
        self.file_name = 'name.mp3'
        self.file_name_new = ''
        self.text = ''
        self.text_new = ''
        self.save = False
        self.lis = False
        self.create = False
        self.compare_url = False
        self.compare_name = False
        self.compare_text = False
        self.compare_voice = False
        self.error = False
        self.status = False
        self.counter = 0
        self.default_url = self.get_url() + '/Text_to_speech'
        self.textEdit_2.setText(self.default_url)

        #cac ham xu li tin hieu
    def getUrl(self):
        url_1 = QFileDialog.getExistingDirectory(None, 'located save file',self.default_url, QFileDialog.ShowDirsOnly) 
        
        if (url_1 == ''):
        	url_1 = self.default_url
        self.textEdit_2.setText(str(url_1))

    def compareURL(self):
    	self.url_new = self.textEdit_2.toPlainText()
    	if (self.url_new == ''):
    			self.url_new = self.default_url
    	if (self.url_new != self.url):
    		self.url = self.url_new
    		self.compare_url = True
    		self.save = False
    	else:
    		self.compare_url = False

    def compareName(self):
    	self.file_name_new = self.textEdit_3.toPlainText()
    	if (self.file_name_new == ''):
            self.file_name_new = 'name.mp3'
        
    	if (self.file_name != self.file_name_new):
    		self.file_name = self.file_name_new
    		self.compare_name = True
    		self.save = False
    	else:
    		self.compare_name = False

    def compareText(self):
    	self.text_new = self.textEdit.toPlainText().encode('utf-8')
    	if (self.text != self.text_new):
    		self.text = self.text_new
    		self.compare_text = True
    		self.create = False
    		self.save = False
    	else:
    		self.compare_text = False

    def compareVoice(self):
    	self.voice_new = self.comboBox.currentText()
    	print(self.voice_new)
    	if (self.voice != self.voice_new):
    		self.voice = self.voice_new
    		self.compare_voice = True
    		self.create = False
    		self.save = False
    	else:
    		self.compare_voice = False

    def createFile(self):
    	self.compareText()
    	self.compareVoice()
        if (self.text == ''):
            print('ban chua nhap gi')
            self.error = True
            self.timer(self.timer_func, 3)
            
        else:
        	self.error = False
        	if (self.compare_text) | (self.compare_voice):
        		self.TTS_gui.api_fpt(self.text, self.voice.encode('utf-8'))
        		for i in range(2):
        			self.TTS_gui.download()
        			time.sleep(1)
        		self.create = True
        		print(self.save)
        		self.timer(self.timer_func, 4)
        print(self.create)
    	print(self.save)
        

    def saveFile(self):
    	self.compareText()
    	self.compareVoice()
    	self.compareName()
    	self.compareURL()
        if (self.text == ''):
            print('ban chua nhap gi')
            self.error = True
            self.timer(self.timer_func, 4)

    	else:
    		self.error = False
    		if (self.create == True):
    			self.TTS_gui.save_to_path(self.url, self.file_name)
    			print('save1')
    			self.save = True
    			self.timer(self.timer_func, 3)
    		else:
				if (self.compare_voice) | (self.compare_text) | (self.compare_name) | (self.compare_url) :
					text = self.text
					self.TTS_gui.api_fpt(text, self.voice.encode('utf-8'))
					print('save2')
					for i in range(2):
						self.TTS_gui.download()
    					time.sleep(1)
    				self.TTS_gui.save_to_path(self.url, self.file_name)
    				self.save = True
    				self.timer(self.timer_func, 3)
    	print(self.create)
    	print(self.save)



    def listenFile(self):
    	self.compareText()
    	self.compareVoice()
    	self.compareName()
    	self.compareURL()
    	if (self.create == True) | (self.save == True):
    		print('listen1')
    		self.TTS_gui.playmp3()
    	else:
    		print('listen2')
    		if (self.text == ''):
    			print('ban chua nhap gi')
    			self.error = True
    			self.timer(self.timer_func, 3)
    		else:
    			self.error = False
    			self.TTS_gui.api_fpt(self.text, self.voice.encode('utf-8'))
    			for i in range(2):
    				self.TTS_gui.download()
    				time.sleep(1)
    				self.create = True
    			self.TTS_gui.playmp3()
    	print(self.create)
    	print(self.save)

    def timer(self,slot, count = 1, interval = 1000):
    	text_change = ''
    	if (self.save == True):
    		text_change = 'luu thanh cong'
    	if (self.create == True) & (self.save == False):
    		text_change = 'tao file thanh cong'
    	if (self.error == True):
    		text_change = 'chua nhap noi dung'
    	self.label_1.setText(text_change)

    	def handler():
    		#nonlocal counter
    		self.counter += 1
    		slot(self.counter)
    		if self.counter >= count:
    			timer.stop()
    			timer.deleteLater()
    			self.counter = 0
    	timer = QTimer(None)
    	timer.timeout.connect(handler)
    	timer.start(interval)

    def timer_func(self, count):
    	if count >= 3:
    		self.label_1.setText('nhap noi dung')
    		#self.textEdit_3.setText('aaaaaaaa')
    	

    def get_url(self):
    	url_new = ''
    	URL = ''
    	url = os.getcwd()
    	URL = url.split("/")
    	for i in URL:
    		if (i != "home") & (i != ''):
    			url_new = "/home/" + i 
    			break
    	return(url_new)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "create file"))
        self.pushButton_2.setText(_translate("MainWindow", "listen"))
        self.textEdit_2.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:22pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:22pt;\">/home</span></p></body></html>"))
        self.pushButton_3.setText(_translate("MainWindow", "save"))
        self.pushButton_4.setText(_translate("MainWindow", "brower"))
       # self.textEdit_6.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
#"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
#"p, li { white-space: pre-wrap; }\n"
#"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:22pt; font-weight:400; font-style:normal;\">\n"
#"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Nhập nội dung</p></body></html>"))

     #   self.textBrowser.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
#"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
#"p, li { white-space: pre-wrap; }\n"
#"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:22pt; font-weight:400; font-style:normal;\">\n"
#"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:22pt;\">Nhập nội dung</span></p></body></html>"))
        self.textEdit_3.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:22pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">name.mp3</p></body></html>"))
        self.textEdit_4.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:22pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:22pt;\">Name</span></p></body></html>"))
        self.textEdit_5.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:22pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:22pt;\">URL</span></p></body></html>"))
        self.label.setText(_translate("MainWindow", "giọng đọc"))
        self.label_1.setText(_translate("MainWindow", "nhap noi dung"))

        self.comboBox.setItemText(0, _translate("MainWindow", "nữ (miền bắc)"))
        self.comboBox.setItemText(1, _translate("MainWindow", "nam (miền bắc)"))