#!/usr/bin/env python
# encoding: utf-8
import requests
import shutil
from playsound import playsound
import os

class Text_to_speech():
	def __init__(self):
		#self.text_VN = ''
		self.link_mp3 = ''
		self.url_mp3_2 = ''
		self.Text_tts = ''
		self.file_name = ''
		self.stastus = False
		self.url_new = ''
		self.current_url = os.getcwd()

	def api_fpt(self, text_VN, voice):
		url = 'https://api.fpt.ai/hmi/tts/v5'
		#text_VN = 'Vương Sỹ Cương'
		text_voice = 'banmai'
		payload = unicode(text_VN,'utf-8')
		#payload = 'Vương Sỹ Cương'
		if (voice == 'nữ (miền bắc)'):
			text_voice = 'banmai'
		if (voice == 'nam (miền bắc)'):
			text_voice = 'leminh'

		headers = {
		'api-key': 'DFwAiSMTqskZDINN1745yK52mqQB1stZ',
		'speed': '',
		'voice': text_voice
		}
		response = requests.request('POST', url, data=payload.encode('utf-8'), headers=headers)
		print(response.text)
		s = response.text
		link = "https:" + s.split(':')[2] 
		self.link_mp3 = link.split(',')[0].replace('"','')
		print(self.link_mp3)

	def download(self):
		#download file mp3
		
		self.createFolder()
		os.chdir(self.current_url + '/Text_to_speech')
		print(self.current_url)

		r = requests.get(self.link_mp3, stream = True) 
		self.file_name = self.link_mp3.split('/')[-1]
		print(self.file_name)
		#r.raise_for_status()
      
		with open(self.file_name, 'wb') as f:  
			for chunk in r.iter_content(chunk_size = 1024, decode_unicode = False): 
				if chunk: 
					f.write(chunk)

		# duong dan file mp3
		url_mp3 = self.current_url + '/Text_to_speech/' + self.file_name
		self.url_mp3_2 = url_mp3.encode('ascii','ignore')
		print(type(self.url_mp3_2))
		self.status = False
		
		#print(url_mp3)

	def save_to_path(self, path, name):
		self.createFolder()
		if (name.split('.')[-1] == 'mp3'):
			name_mp3 = name
		else:
			name_mp3 = name + '.mp3'

		#di chuyen file den thuc muc chi dinh
		#path = "/home/zero/catkin_ws/src/cmc_robot_daily_work/GUI-app_v1/voice/"
		if (path == '/home'):
			path1 = path + '/zero'
		else:
			path1 = path

		if (path1[-1] == '/'):
			self.url_new = path1 + name_mp3
		else:
			self.url_new = path1 + '/' + name_mp3
		self.url_new = self.url_new.encode('ascii','ignore')
		# name + '.mp3'
		print(self.url_new)
		shutil.copy(self.url_mp3_2, self.url_new)
		self.stastus = True


	def playmp3(self):
		#if (self.stastus == True):
		#	playsound(self.url_new)

		if (self.stastus == False):
			playsound(self.url_mp3_2)

	def deleteFile(self):
		if (os.path.exists(self.current_url + '/Text_to_speech')):
			os.remove(self.current_url + '/Text_to_speech')
		else:
			return

	def createFolder(self):
		url_new = ''
		URL = ''
		url = os.getcwd()
		URL = url.split("/")
		for i in URL:
			if (i != "home") & (i != ''):
				url_new = "/home/" + i 
				print(type(url_new))
				break

		url_new = url_new +'/Text_to_speech'
		if (os.path.exists(url_new)):
			return
		else:
			os.mkdir(url_new)

#def main():
#	test_1 = Text_to_speech()
#	test_1.entryText()
#main()