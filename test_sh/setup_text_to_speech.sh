﻿## cai bang lenh : sudo bash setup_text_to_speech.sh




#!/bin/bash

bash -i -c "git clone https://gitlab.com/cuong1997cuo/text_to_speech.git"
STR=""         
IN=$PWD
mails=$(echo $IN | tr "/" "\n")
for addr in $mails
do
    echo "> [$addr]"
    if [ $addr != "home" ]; then
          STR="/home/$addr/Desktop"  
          break
    fi  
done

echo "#!/usr/bin/env xdg-open" > $STR/Text_to_speech.desktop
echo "[Desktop Entry]" >> $STR/Text_to_speech.desktop
echo "Version=1.0" >> $STR/Text_to_speech.desktop
echo "Type=Application" >> $STR/Text_to_speech.desktop
echo "Terminal=true" >> $STR/Text_to_speech.desktop
echo "Icon=$PWD/text_to_speech/setup/icon/icon.jpeg" >> $STR/Text_to_speech.desktop
echo "Exec=bash -c \"cd $PWD/text_to_speech/setup && python main_ui.py\"" >> $STR/Text_to_speech.desktop
echo "Name=APP_text_to_speech" >> $STR/Text_to_speech.desktop
echo "Comment=convert text to speech" >> $STR/Text_to_speech.desktop

cd $STR
sudo chmod +x Text_to_speech.desktop


